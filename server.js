// Require dependencies
const express = require('express');
const app = express();
const socket = require('socket.io');
const PORT = process.env.PORT || 9090

// Create Simple HTTP Server with Express
const server = app.listen(PORT, () => console.log(`Listening on ${ PORT }`));
  
app.use(express.static('public'));

// Create a Socket.io server
const io = socket(server);

// Handle Socket Connections
io.on('connection', (socket) => {
    console.log('Client connected');
    socket.on('disconnect', () => console.log('Client disconnected'));
});

// Broadcast updates
setInterval(() => io.emit('time', new Date().toTimeString()), 1000);